package FordFulkerson;

import java.util.ArrayList;

public class FlowNetwork {
    private int V;
    private int E;
    private ArrayList<FlowEdge>[] adj;

    public FlowNetwork(int[] x, int y[], int E, int n1, int n2) {
        this.V = n1 + n2 + 3;
        this.E = E;
        adj = new ArrayList[V];
        for (int v = 0; v < V; v++)
            adj[v] = new ArrayList<>();

        for (int i = 0; i < E; i++) {
            int v = x[i];
            int w = y[i];
            double capacity = Double.POSITIVE_INFINITY;
            addEdge(new FlowEdge(v, n1 + w, capacity));

        }

        for (int i = 0; i < n1; i++) {
            addEdge(new FlowEdge(V - 2, i, 1.0));
        }
        for (int i = 0; i < n2; i++)
            addEdge(new FlowEdge(i + n1, V - 1, 1.0));
    }


    // number of vertices and edges
    public int V() {
        return V;
    }


    // add edge e in both v's and w's adjacency lists
    public void addEdge(FlowEdge e) {
        int v = e.from();
        int w = e.to();

        adj[v].add(e);
        adj[w].add(e);
    }

    // return list of edges incident to  v
    public Iterable<FlowEdge> adj(int v) {
        return adj[v];
    }

}








