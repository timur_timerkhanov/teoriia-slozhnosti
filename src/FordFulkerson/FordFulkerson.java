package FordFulkerson;

public class FordFulkerson {
    private boolean[] marked;     // marked[v] = true если путь s->v в остаточном графе
    private FlowEdge[] edgeTo;    // edgeTo[v] = последнее ребро на кратчайшем остаточном пути
    private double value;         // текущее значение максимального потока

    // Максимальный поток в сети G из истока в сток
    public FordFulkerson(FlowNetwork G, int s, int t) {
        value = excess(G, t);
        // Пока существует увеличивающий путь, используем его
        while (hasAugmentingPath(G, s, t)) {

            // пропускная способность самого "узкого" места
            double bottle = Double.POSITIVE_INFINITY;
            for (int v = t; v != s; v = edgeTo[v].other(v)) {
                bottle = Math.min(bottle, edgeTo[v].residualCapacityTo(v));
            }

            // наполняем поток
            for (int v = t; v != s; v = edgeTo[v].other(v)) {
                edgeTo[v].addResidualFlowTo(v, bottle);
            }

            value += bottle;
        }

    }

    // Возвращает значение максимального потока
    public double value() {
        return value;
    }




    // Если существует увеличивающий путь, возвращает его, иначе null
    private boolean hasAugmentingPath(FlowNetwork G, int s, int t) {
        edgeTo = new FlowEdge[G.V()];
        marked = new boolean[G.V()];

        // Поиск в ширину
        Queue<Integer> q = new Queue<Integer>();
        q.enqueue(s);
        marked[s] = true;
        while (!q.isEmpty()) {
            int v = q.dequeue();

            for (FlowEdge e : G.adj(v)) {
                int w = e.other(v);

                // Если остаточная емкость из v в w больше нуля
                if (e.residualCapacityTo(w) > 0) {
                    if (!marked[w]) {
                        edgeTo[w] = e;
                        marked[w] = true;
                        q.enqueue(w);
                    }
                }
            }
        }

        return marked[t];
    }


    // return excess flow at vertex v
    private double excess(FlowNetwork G, int v) {
        double excess = 0.0;
        for (FlowEdge e : G.adj(v)) {
            if (v == e.from()) excess -= e.flow();
            else excess += e.flow();
        }
        return excess;
    }


}
