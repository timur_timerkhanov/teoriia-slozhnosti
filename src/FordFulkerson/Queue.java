package FordFulkerson;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements Iterable<T> {
    private int N;         // number of elements on queue
    private Node<T> first;    // beginning of queue
    private Node<T> last;     // end of queue

    // helper linked list class
    private static class Node<T> {
        public Node() {
        }

        public T item;
        public Node<T> next;
    }

    /**
     * 042         * Create an empty queue.
     * 043
     */
    public Queue() {
        first = null;
        last = null;
        N = 0;
    }

    /**
     * 051         * Is the queue empty?
     * 052
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * 058         * Return the number of items in the queue.
     * 059
     */
    public int size() {
        return N;
    }

    /**
     * 065         * Return the item least recently added to the queue.
     * 066         * @throws java.util.NoSuchElementException if queue is empty.
     * 067
     */
    public T peek() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        return first.item;
    }

    /**
     * 074         * Add the item to the queue.
     * 075
     */
    public void enqueue(T item) {
        Node<T> oldlast = last;
        last = new Node<>();
        last.item = item;
        last.next = null;
        if (isEmpty()) first = last;
        else oldlast.next = last;
        N++;
    }

    /**
     * 087         * Remove and return the item on the queue least recently added.
     * 088         * @throws java.util.NoSuchElementException if queue is empty.
     * 089
     */
    public T dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        T item = first.item;
        first = first.next;
        N--;
        if (isEmpty()) last = null;
        return item;
    }

    /**
     * 100         * Return string representation.
     * 101
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (T item : this)
            s.append(item + " ");
        return s.toString();
    }

    // check internal invariants
    private static <T> boolean check(Queue<T> that) {
        int N = that.N;
        Queue.Node<T> first = that.first;
        Queue.Node<T> last = that.last;
        if (N == 0) {
            if (first != null) return false;
            if (last != null) return false;
        } else if (N == 1) {
            if (first == null || last == null) return false;
            if (first != last) return false;
            if (first.next != null) return false;
        } else {
            if (first == last) return false;
            if (first.next == null) return false;
            if (last.next != null) return false;

            // check internal consistency of instance variable N
            int numberOfNodes = 0;
            for (Queue.Node<T> x = first; x != null; x = x.next) {
                numberOfNodes++;
            }
            if (numberOfNodes != N) return false;

            // check internal consistency of instance variable last
            Queue.Node<T> lastNode = first;
            while (lastNode.next != null) {
                lastNode = lastNode.next;
            }
            if (last != lastNode) return false;
        }

        return true;
    }


    public Iterator<T> iterator() {
        return new ListIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator implements Iterator<T> {
        private Node<T> current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T item = current.item;
            current = current.next;
            return item;
        }
    }
}













