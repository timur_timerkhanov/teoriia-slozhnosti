import java.util.ArrayList;

public class Graph {
    public final int NIL = 0;
    public final int INF = Integer.MAX_VALUE;
    // Список смежности
    private ArrayList<Integer>[] Adj;
    // Вершина, которая является предыдущей на кратчайшем пути
    private int[] Pair;
    // Кратчайший путь до вершины
    private int[] distTo;
    // Количество вершин в правой и левых долях
    public int n1, n2;

    public Graph(int n1, int n2) {
        this.n1 = n1;
        this.n2 = n2;
        Pair = new int[n1 + n2 + 2];
        distTo = new int[n1 + n2 + 2];
    }


    /**
     * Function to make h vtices x , y
     **/
    public Graph makeGraph(int[] x, int[] y, int E) {
        Adj = new ArrayList[n1 + n2 + 2];
        for (int i = 0; i < Adj.length; ++i)
            Adj[i] = new ArrayList<>();
        /* adding edges */
        for (int i = 0; i < E; ++i)
            addEdge(x[i] + 1, y[i] + 1);
        return this;
    }


    /**
     * Function to add a edge
     **/
    public void addEdge(int u, int v) {
        Adj[u].add(n1 + v);
        Adj[n1 + v].add(u);
    }


    public ArrayList<Integer>[] getAdj() {
        return Adj;
    }


    public int[] getPairArray() {
        return Pair;
    }


    public int[] getDistToArray() {
        return distTo;
    }


    public int getN1() {
        return n1;
    }


    public int getN2() {
        return n2;
    }

}
