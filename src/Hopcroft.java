import java.util.LinkedList;
import java.util.Queue;

public class Hopcroft {
    private Graph graph;

    public Hopcroft(Graph graph) {
        this.graph = graph;
    }

    // Поиск в ширину
    private boolean BFS() {
        Queue<Integer> queue = new LinkedList<>();

        for (int v = 1; v <= graph.getN1(); ++v) {
            if (graph.getPairArray()[v] == graph.NIL) {
                graph.getDistToArray()[v] = 0;
                queue.add(v);
            } else
                graph.getDistToArray()[v] = graph.INF;
        }
        graph.getDistToArray()[graph.NIL] = graph.INF;

        while (!queue.isEmpty()) {
            int v = queue.poll();
            if (graph.getDistToArray()[v] < graph.getDistToArray()[graph.NIL])
                for (int u : graph.getAdj()[v]) {
                    if (graph.getDistToArray()[graph.getPairArray()[u]] == graph.INF) {
                        graph.getDistToArray()[graph.getPairArray()[u]] = graph.getDistToArray()[v] + 1;
                        queue.add(graph.getPairArray()[u]);
                    }
                }
        }
        return graph.getDistToArray()[graph.NIL] != graph.INF;
    }

    // Поиск в глубину
    private boolean DFS(int v) {
        if (v != graph.NIL) {
            for (int u : graph.getAdj()[v])
                if (graph.getDistToArray()[graph.getPairArray()[u]] == graph.getDistToArray()[v] + 1)
                    if (DFS(graph.getPairArray()[u]))
                    {
                        graph.getPairArray()[u] = v;
                        graph.getPairArray()[v] = u;
                        return true;
                    }

            graph.getDistToArray()[v] = graph.INF;
            return false;
        }
        return true;
    }

    public int HopcroftKarp() {
        int matching = 0;
        // Поиск в ширину, находим свободные вершины из V на уровне k
        while (BFS())
            // Находим максимальное множество непересекающихся по вершинам путей длины k
            for (int v = 1; v <= graph.getN1(); ++v)
                if (graph.getPairArray()[v] == graph.NIL)
                    if (DFS(v))
                        matching = matching + 1;
        // Увеличиваем значение паросочетания
        return matching;
    }
}

