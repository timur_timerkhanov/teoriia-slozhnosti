import java.util.ArrayList;
import java.util.Arrays;

public class Kun {
    private int n1;
    private int n2;
    private ArrayList<Integer> adj[];
    private boolean[] visited;
    private int matching;
    private int[] maxEdge;

    public Kun(Graph graph) {
        this.n1 = graph.getN1();
        this.n2 = graph.getN2();
        this.adj = graph.getAdj();
        this.matching = 0;
        visited = new boolean[n1+1];
        Arrays.fill(visited, false);
        maxEdge = new int[n1 + 4 + n2];
        Arrays.fill(maxEdge, -1);
    }

    private boolean kunhs(int v) {
        // Проверяем, были ли мы уже в этой вершине или нет, если были, то выходим из метода и возвращаем false
        if (visited[v]) {
            return false;
        }
        // Если не были, то помечаем, что были
        visited[v] = true;

        //  По всем соседям данной вершины из второй доли
        for (int i = 0; i < adj[v].size(); ++i) {
            int w = adj[v].get(i);
            // Если найдем увеличивающую цепь, то добавляем это ребро в maxEdge
            // Проверяем, есть ли у w смежное ребро или существует увеличивающий путь из w
            if (maxEdge[w] == -1 || kunhs(maxEdge[w])) {
                maxEdge[w] = v;
                return true;
            }
        }
        return false;
    }

    public int maxCombination() {
        // Находим максимальное паросочетание
        for (int v = 1; v <= n1; v++) {
            Arrays.fill(visited, false);
            // Если нашли увеличивающую цепь, увеличиваем matching на 1
            if (kunhs(v)) {
                matching++;
            }
        }
        return matching;
    }
}
