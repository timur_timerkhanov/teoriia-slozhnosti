import java.io.IOException;

public class Main {

    private final static int N1 = 50;
    private final static int N2 = 50;
    private final static int E = 1870;

    public static void main(String[] args) throws IOException {
        TestingService testingService = new TestingService();
        testingService.fullTest(N1, N2, E);
    }
}
