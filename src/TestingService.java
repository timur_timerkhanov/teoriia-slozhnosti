import FordFulkerson.FlowNetwork;
import FordFulkerson.FordFulkerson;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.LongStream;

public class TestingService {
    ArrayList<Integer>[] adj;
    String basePackage = "/home/timur/IdeaProjects/HopcroftKarpAlgorithm/src/TestGraphs";

    public void createAndWriteRandomGraph(int n1, int n2, int E) throws IOException {
        Random random = new Random();
        String textFormatGraph = "";
        adj = new ArrayList[n1 + 1];
        for (int i = 0; i <= n1; ++i) {
            adj[i] = new ArrayList<>();
        }

        for (int i = 0; i < E; i++) {
            int u = random.nextInt(n1);
            int v = random.nextInt(n2);
            if (!edgeExists(u, v)) {
                textFormatGraph += u + " " + v + "\n";
                adj[u].add(v);
            } else {
                i--;
            }
        }
        writeToFile(textFormatGraph, n1, n2, E);
    }

    public boolean edgeExists(int v, int u) {
        for (int i = 0; i < adj[v].size(); i++) {
            if (adj[v].get(i) == u)
                return true;
        }
        return false;
    }

    public void writeToFile(String word, int n1, int n2, int E) throws IOException {
        String filename = "/" + n1 + "-" + n2 + "-" + E + ".txt";
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(basePackage + filename));
        bufferedWriter.write(word + "\n");
        bufferedWriter.close();
    }

    public Graph graphFromFile(int n1, int n2, int E) throws IOException {
        Graph graph = new Graph(n1, n2);
        String filename = "/" + n1 + "-" + n2 + "-" + E + ".txt";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(basePackage + filename));
        String sCurrentLine;

        int x[] = new int[E];
        int y[] = new int[E];
        for (int i = 0; i < E; i++) {
            sCurrentLine = bufferedReader.readLine();
            String[] parts = sCurrentLine.split(" ");
            x[i] = Integer.valueOf(parts[0]);
            y[i] = Integer.valueOf(parts[1]);
        }

        return graph.makeGraph(x, y, E);
    }

    public FlowNetwork flowNetworkFromFile(int n1, int n2, int E) throws IOException {
        String filename = "/" + n1 + "-" + n2 + "-" + E + ".txt";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(basePackage + filename));
        String sCurrentLine;

        int x[] = new int[E + n1];
        int y[] = new int[E + n2 ];
        for (int i = 0; i < E; i++) {
            sCurrentLine = bufferedReader.readLine();
            String[] parts = sCurrentLine.split(" ");
            x[i] = Integer.valueOf(parts[0]);
            y[i] = Integer.valueOf(parts[1]);
        }
        FlowNetwork flowNetwork = new FlowNetwork(x, y, E, n1, n2);
        return flowNetwork;
    }

    public void fullTest(int n1, int n2, int E) throws IOException {

        int Times = 10;

        long[] HopcroftTimes = new long[Times];
        long[] KunhTimes = new long[Times];
        long[] FulkersonTimes = new long[Times];

        for (int i = 0; i < Times; i++) {
            createAndWriteRandomGraph(n1, n2, E);
            Graph graph = graphFromFile(n1, n2, E);
            FlowNetwork flowNetwork = flowNetworkFromFile(n1, n2, E);

            // Алгоритм Хопкрофта-Карпа
            long startTime = System.nanoTime();
            Hopcroft hc = new Hopcroft(graph);
            System.out.println("\nMatches : " + hc.HopcroftKarp());
            long stopTime = System.nanoTime();
            HopcroftTimes[i] = stopTime - startTime;

            // Алгоритм Куна
            long startTime1 = System.nanoTime();
            Kun kun = new Kun(graph);
            System.out.println("ПО куну! " + kun.maxCombination());
            long stopTime1 = System.nanoTime();
            KunhTimes[i] = stopTime1 - startTime1;

            // Алгоритм Форда-Фалкерсона
            // Источник и сток
            int s = n1 + n2 + 1, t = n1 + n2 + 2;
            long startTime2 = System.nanoTime();
            FordFulkerson maxflow = new FordFulkerson(flowNetwork, s, t);
            long stopTime2 = System.nanoTime();

            System.out.println("Форд-Фалкерсон = " + (int) maxflow.value());
            FulkersonTimes[i] = stopTime2 - startTime2;

        }

        long KunhsAverage = LongStream.of(KunhTimes).sum() / Times;
        long HopcroftAverage = LongStream.of(HopcroftTimes).sum() / Times;
        long FulkersonAverage = LongStream.of(FulkersonTimes).sum() / Times;


        System.out.println("Кун " + String.format("%.3f", KunhsAverage * 0.0000001));
        System.out.println("Хопкрофт " + String.format("%.3f", HopcroftAverage * 0.0000001));
        System.out.println("Фалкерсон " + String.format("%.3f", FulkersonAverage * 0.0000001));

    }


}
